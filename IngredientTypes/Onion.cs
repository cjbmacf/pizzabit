﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.IngredientTypes
{
    public class Onion : IIngredient
    {
        public Onion()
        {
            Name = "Onion";
        }

        double IIngredient.GetPrice()
        {
            return 0.2;
        }

        protected string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public override string ToString()
        {
            return Name;
        }

        int IIngredient.MaxPortion()
        {
            return 1;
        }
    }
}

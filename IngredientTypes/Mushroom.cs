﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.IngredientTypes
{
    public class Mushroom : IIngredient
    {
        public Mushroom()
        {
            Name = "Mushroom";
        }

        double IIngredient.GetPrice()
        {
            return 0.5;
        }

        protected string _name;
        string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public override string ToString()
        {
            return Name;
        }

        int IIngredient.MaxPortion()
        {
            return 1;
        }
    }
}

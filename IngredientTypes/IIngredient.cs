﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    interface IIngredient
    {
        /// <summary>
        /// Return the price of the ingredient
        /// </summary>
        /// <returns></returns>
        double GetPrice();

        /// <summary>
        /// Return maximum number of portions per pizza
        /// </summary>
        /// <returns></returns>
        int MaxPortion();

        ///// <summary>
        ///// Gets or sets name string
        ///// </summary>
        //string Name { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Xml;

namespace WindowsFormsApplication1
{
    class Pizza
    {
        //public Pizza() { }

        public Pizza(Inventory pInventory)
        {
            _inventory = pInventory;
            _ingredients = new List<IIngredient>();
        }

        protected double unit_dough_price = 0.5;
        public double unitDoughPrice
        {
            get { return unit_dough_price; }
            set { unit_dough_price = value; }
        }

        protected PizzaSizes _pizzaSize;
        public PizzaSizes PizzaSize
        {
            get { return _pizzaSize; }
            set { _pizzaSize = value; }
        }

        protected Inventory _inventory;
        public Inventory Inventory
        {
            get { return _inventory; }
            set { _inventory = value; }
        }

        protected ICrustType _crustType;
        public ICrustType CrustType
        {
            get { return _crustType; }
            set { _crustType = value; }
        }

        protected List<IIngredient> _ingredients;
        public List<IIngredient> Ingredients
        {
            get { return _ingredients; }
            set { _ingredients = value; }
        }

        public void AddIngredient(IIngredient pIngredient)
        {
            _ingredients.Add(pIngredient);
        }

        public int GetPortions(IIngredient pIngredient)
        {
            int? portion = null;

            if (String.IsNullOrEmpty(pIngredient.ToString()))
            {
                portion = 0;
                return 0;
            }
            else
            {
                switch (pIngredient.ToString())
                {
                    case "Mushroom":
                        portion = pIngredient.MaxPortion();
                        return (int)portion;
                    case "Onion":
                        portion = pIngredient.MaxPortion();
                        return (int)portion;
                    case "Beef":
                        portion = pIngredient.MaxPortion();
                        return (int)portion;
                    case "Chicken":
                        portion = pIngredient.MaxPortion();
                        return (int)portion;
                    default:
                        break;
                }
                return (int)portion;
            }
        }

        public int AddPortions(IIngredient pIngredient)
        {
            int count = 0;

            foreach (IIngredient pIng in _ingredients)
            {
                if (pIng.ToString() == pIngredient.ToString())
                {
                    count++;
                }
            }
            return count;
        }

        public double GetPrice()
        {
            double price;
            price = unit_dough_price;

            foreach (IIngredient ingredient in _ingredients)
            {
                price = price + ingredient.GetPrice();
            }

            price = price + CrustType.GetPrice();
            price = price + PizzaSize.GetPrice(price);

            return price;
        }
    }
}

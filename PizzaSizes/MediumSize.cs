﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1//.PizzaSizes
{
    public class MediumSize : PizzaSizes
    {
        public MediumSize()
        {
            Name = "Medium";
        }

        protected string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public override string ToString()
        {
            return Name;
        }

        public double GetPrice(double price)
        {
            price = price * 0.3;
            return price;
        }

        int PizzaSizes.CentimetersDiameter()
        {
            return 25;
        }

        int PizzaSizes.InchesDiameter()
        {
            return 10;
        }
    }
}

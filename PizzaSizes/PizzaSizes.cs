﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    interface PizzaSizes
    {
        /// <summary>
        /// Returns pizza diameter in inches
        /// </summary>
        /// <returns>Integer represntation of inches</returns>
        int InchesDiameter();

        /// <summary>
        /// Returns pizza diameter in centimeters
        /// </summary>
        /// <returns>Integer representation of centimeters</returns>
        int CentimetersDiameter();

        /// <summary>
        /// Returns additional cost
        /// </summary>
        /// <returns>price + additional dough cost</returns>
        double GetPrice(double price);

        ///// <summary>
        ///// Gets or sets name string
        ///// </summary>
        //string Name { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1//.PizzaSizes
{
    class LargeSize : PizzaSizes
    {
        public LargeSize()
        {
            Name = "Large";
        }

        protected string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public override string ToString()
        {
            return Name;
        }

        public double GetPrice(double price)
        {
            price = price * 0.7;
            return price;
        }

        int PizzaSizes.CentimetersDiameter()
        {
            return 35;
        }

        int PizzaSizes.InchesDiameter()
        {
            return 14;
        }
    }
}

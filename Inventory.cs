﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class Inventory
    {
        private int _cheeseportions = 0;
        public int CheesePortions
        {
            get { return _cheeseportions; }
            set { _cheeseportions = value; }
        }

        private int _mushroomportions = 0;
        public int MushroomPortions
        {
            get { return _mushroomportions; }
            set { _mushroomportions = value; }
        }

        private int _onionportions = 0;
        public int OnionPortions
        {
            get { return _onionportions; }
            set { _onionportions = value; }
        }

        private int _beefportions = 0;
        public int BeefPortions
        {
            get { return _beefportions; }
            set { _beefportions = value; }
        }

        private int _chickenportions = 0;
        public int ChickenPortions
        {
            get { return _chickenportions; }
            set { _chickenportions = value; }
        }

        public Inventory()
        {
            CheesePortions = 2;
            MushroomPortions = 2;
            OnionPortions = 2;
            BeefPortions = 2;
            ChickenPortions = 2;
        }
    }
}

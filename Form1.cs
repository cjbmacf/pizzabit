﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.IngredientTypes;

using System.Xml;
using System.IO;
using System.Xml.Serialization;
using System.Security.Permissions;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {

        Pizza pizza;
        string env = @"C:\Users\Conor\Desktop\temp";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            btnAddMushroom.Enabled = false;
            btnAddOnion.Enabled = false;
            btnAddBeef.Enabled = false;
            btnAddChicken.Enabled = false;
            btnCalculatePrice.Enabled = false;
            comboPizzaCrustType.Enabled = false;
            comboPizzaSize.Enabled = false;
            btnSavePizza.Enabled = false;

            lblMushroomPortionsAmount.Text = "0";
            lblOnionPortionsAmount.Text = "0";
            lblBeefPortionsAmount.Text = "0";
            lblChickenPortionsAmount.Text = "0";
            lblPriceAmount.Text = "£" + "0.00";

            comboPizzaSize.Items.Add(new MediumSize());
            comboPizzaSize.Items.Add(new ItalianSize());
            comboPizzaSize.Items.Add(new LargeSize());

            comboPizzaCrustType.Items.Add(new ThinCrustType());
            comboPizzaCrustType.Items.Add(new ThickCrustType());
            comboPizzaCrustType.Items.Add(new CheeseCrustType());
        }

        private void btnAddMushroom_Click(object sender, EventArgs e)
        {
            if (pizza.Inventory.MushroomPortions > 0)
            {
                IIngredient pMushroom = new Mushroom();
                pizza.AddIngredient(pMushroom);
                lblMushroomPortionsAmount.Text = pizza.GetPortions(pMushroom).ToString() + "/" + pMushroom.MaxPortion(); 
            }
            else
            {
                MessageBox.Show("You cannot add anymore mushrooms.");
            }
        }

        private void btnAddOnion_Click(object sender, EventArgs e)
        {
            if (pizza.Inventory.OnionPortions > 0)
            {
                IIngredient pOnion = new Onion();
                pizza.AddIngredient(pOnion);
                lblOnionPortionsAmount.Text = pizza.GetPortions(pOnion).ToString() + "/" + pOnion.MaxPortion();
            }
            else
            {
                MessageBox.Show("You cannot add anymore onions.");
            }
        }

        private void btnAddBeef_Click(object sender, EventArgs e)
        {
            if (pizza.Inventory.BeefPortions > 0)
            {
                IIngredient pBeef = new Beef();
                pizza.AddIngredient(pBeef);
                lblBeefPortionsAmount.Text = pizza.GetPortions(pBeef).ToString() + "/" + pBeef.MaxPortion();
            }
            else
            {
                MessageBox.Show("You cannot add anymore beef.");
            }
        }

        private void btnAddChicken_Click(object sender, EventArgs e)
        {
            if (pizza.Inventory.ChickenPortions > 0)
            {
                IIngredient pChicken = new Chicken();
                pizza.AddIngredient(pChicken);
                lblChickenPortionsAmount.Text = pizza.GetPortions(pChicken).ToString() + "/" + pChicken.MaxPortion();
            }
            else
            {

                MessageBox.Show("You cannot add anymore chicken.");
            }
        }

        private void btnCreateNewPizza_Click(object sender, EventArgs e)
        {
            Inventory inventory = new Inventory();
            pizza = new Pizza(inventory);

            comboPizzaSize.SelectedItem = null;
            comboPizzaCrustType.SelectedItem = null;
            lblPriceAmount.Text = "£" + "0.00";

            if (comboPizzaCrustType.Enabled == false && comboPizzaSize.Enabled == false)
            {
                comboPizzaCrustType.Enabled = true;
                comboPizzaSize.Enabled = true; 
            }
        }

        private void btnCalculatePrice_Click(object sender, EventArgs e)
        {
            lblPriceAmount.Text = "£" + pizza.GetPrice();
        }

        private void comboPizzaCrustType_SelectedIndexChanged(object sender, EventArgs e)
        {
            pizza.CrustType = (ICrustType)comboPizzaCrustType.SelectedItem;
            comboCheckEmpty(comboPizzaCrustType, comboPizzaSize);
        }

        private void comboPizzaSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            pizza.PizzaSize = (PizzaSizes)comboPizzaSize.SelectedItem;
            comboCheckEmpty(comboPizzaCrustType, comboPizzaSize);
        }

        private void comboCheckEmpty(ComboBox combo1, ComboBox combo2)
        {
            if (!String.IsNullOrEmpty(combo1.Text) && !String.IsNullOrEmpty(combo2.Text))
            {
                btnAddMushroom.Enabled = true;
                btnAddOnion.Enabled = true;
                btnAddBeef.Enabled = true;
                btnAddChicken.Enabled = true;
                btnCalculatePrice.Enabled = true;
                btnSavePizza.Enabled = true;
            }
        }

        private void comboCheckEmpty(ComboBox combo1)
        {
            if (!String.IsNullOrEmpty(combo1.Text))
            {
                btnAddMushroom.Enabled = true;
                btnAddOnion.Enabled = true;
                btnAddBeef.Enabled = true;
                btnAddChicken.Enabled = true;
                btnCalculatePrice.Enabled = true;
            }
        }

        private void WriteXmlFile<T>(string filePath, T objectToWrite, bool append = false) where T : new()
        {
            TextWriter writer = null;
            try
            {
                //FileIOPermission f = new FileIOPermission(FileIOPermissionAccess.Write, env);
                //f.AllLocalFiles = FileIOPermissionAccess.Read;
                //f.AddPathList(FileIOPermissionAccess.Read ,env);

                var serializer = new XmlSerializer(typeof(T));
                writer = new StreamWriter(filePath, append);
                serializer.Serialize(writer, objectToWrite);
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
        }

        private void btnSavePizza_Click(object sender, EventArgs e)
        {
            string ingr = String.Join(",", pizza.Ingredients);
            string crust = comboPizzaCrustType.SelectedItem.ToString();
            string size = comboPizzaSize.SelectedItem.ToString();
            MessageBox.Show(ingr + ", Crust Type: " + crust + ", Size: " + size);
            WriteXmlFile(env, pizza, true);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class CheeseCrustType : ICrustType
    {
        public CheeseCrustType()
        {
            Name = "Cheese crust";
        }

        double ICrustType.GetPrice()
        {
            return 9;
        }

        protected string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public override string ToString()
        {
            return Name;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    interface ICrustType
    {
        /// <summary>
        /// Return the price of the crust type
        /// </summary>
        double GetPrice();

        ///// <summary>
        ///// Gets or sets name string
        ///// </summary>
        //string Name { get; set; }
    }
}

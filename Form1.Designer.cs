﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAddMushroom = new System.Windows.Forms.Button();
            this.btnAddOnion = new System.Windows.Forms.Button();
            this.btnAddBeef = new System.Windows.Forms.Button();
            this.btnAddChicken = new System.Windows.Forms.Button();
            this.btnCreateNewPizza = new System.Windows.Forms.Button();
            this.btnCalculatePrice = new System.Windows.Forms.Button();
            this.lblMushroomPortions = new System.Windows.Forms.Label();
            this.lblOnionPortions = new System.Windows.Forms.Label();
            this.lblBeefPortions = new System.Windows.Forms.Label();
            this.lblChickenPortions = new System.Windows.Forms.Label();
            this.lblCalculatedPrice = new System.Windows.Forms.Label();
            this.lblMushroomPortionsAmount = new System.Windows.Forms.Label();
            this.lblOnionPortionsAmount = new System.Windows.Forms.Label();
            this.lblBeefPortionsAmount = new System.Windows.Forms.Label();
            this.lblChickenPortionsAmount = new System.Windows.Forms.Label();
            this.lblPriceAmount = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboPizzaCrustType = new System.Windows.Forms.ComboBox();
            this.comboPizzaSize = new System.Windows.Forms.ComboBox();
            this.btnSavePizza = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnAddMushroom
            // 
            this.btnAddMushroom.Location = new System.Drawing.Point(287, 91);
            this.btnAddMushroom.Name = "btnAddMushroom";
            this.btnAddMushroom.Size = new System.Drawing.Size(95, 23);
            this.btnAddMushroom.TabIndex = 1;
            this.btnAddMushroom.Text = "Add Mushrooms";
            this.btnAddMushroom.UseVisualStyleBackColor = true;
            this.btnAddMushroom.Click += new System.EventHandler(this.btnAddMushroom_Click);
            // 
            // btnAddOnion
            // 
            this.btnAddOnion.Location = new System.Drawing.Point(287, 121);
            this.btnAddOnion.Name = "btnAddOnion";
            this.btnAddOnion.Size = new System.Drawing.Size(95, 23);
            this.btnAddOnion.TabIndex = 2;
            this.btnAddOnion.Text = "Add Onions";
            this.btnAddOnion.UseVisualStyleBackColor = true;
            this.btnAddOnion.Click += new System.EventHandler(this.btnAddOnion_Click);
            // 
            // btnAddBeef
            // 
            this.btnAddBeef.Location = new System.Drawing.Point(287, 151);
            this.btnAddBeef.Name = "btnAddBeef";
            this.btnAddBeef.Size = new System.Drawing.Size(95, 23);
            this.btnAddBeef.TabIndex = 3;
            this.btnAddBeef.Text = "Add Beef";
            this.btnAddBeef.UseVisualStyleBackColor = true;
            this.btnAddBeef.Click += new System.EventHandler(this.btnAddBeef_Click);
            // 
            // btnAddChicken
            // 
            this.btnAddChicken.Location = new System.Drawing.Point(287, 181);
            this.btnAddChicken.Name = "btnAddChicken";
            this.btnAddChicken.Size = new System.Drawing.Size(95, 23);
            this.btnAddChicken.TabIndex = 4;
            this.btnAddChicken.Text = "Add Chicken";
            this.btnAddChicken.UseVisualStyleBackColor = true;
            this.btnAddChicken.Click += new System.EventHandler(this.btnAddChicken_Click);
            // 
            // btnCreateNewPizza
            // 
            this.btnCreateNewPizza.Location = new System.Drawing.Point(267, 280);
            this.btnCreateNewPizza.Name = "btnCreateNewPizza";
            this.btnCreateNewPizza.Size = new System.Drawing.Size(115, 23);
            this.btnCreateNewPizza.TabIndex = 5;
            this.btnCreateNewPizza.Text = "Create New Pizza";
            this.btnCreateNewPizza.UseVisualStyleBackColor = true;
            this.btnCreateNewPizza.Click += new System.EventHandler(this.btnCreateNewPizza_Click);
            // 
            // btnCalculatePrice
            // 
            this.btnCalculatePrice.Location = new System.Drawing.Point(287, 211);
            this.btnCalculatePrice.Name = "btnCalculatePrice";
            this.btnCalculatePrice.Size = new System.Drawing.Size(95, 23);
            this.btnCalculatePrice.TabIndex = 6;
            this.btnCalculatePrice.Text = "Calculate Price";
            this.btnCalculatePrice.UseVisualStyleBackColor = true;
            this.btnCalculatePrice.Click += new System.EventHandler(this.btnCalculatePrice_Click);
            // 
            // lblMushroomPortions
            // 
            this.lblMushroomPortions.AutoSize = true;
            this.lblMushroomPortions.Location = new System.Drawing.Point(12, 96);
            this.lblMushroomPortions.Name = "lblMushroomPortions";
            this.lblMushroomPortions.Size = new System.Drawing.Size(102, 13);
            this.lblMushroomPortions.TabIndex = 8;
            this.lblMushroomPortions.Text = "Mushrooms Portions";
            // 
            // lblOnionPortions
            // 
            this.lblOnionPortions.AutoSize = true;
            this.lblOnionPortions.Location = new System.Drawing.Point(12, 126);
            this.lblOnionPortions.Name = "lblOnionPortions";
            this.lblOnionPortions.Size = new System.Drawing.Size(81, 13);
            this.lblOnionPortions.TabIndex = 9;
            this.lblOnionPortions.Text = "Onions Portions";
            // 
            // lblBeefPortions
            // 
            this.lblBeefPortions.AutoSize = true;
            this.lblBeefPortions.Location = new System.Drawing.Point(12, 156);
            this.lblBeefPortions.Name = "lblBeefPortions";
            this.lblBeefPortions.Size = new System.Drawing.Size(70, 13);
            this.lblBeefPortions.TabIndex = 10;
            this.lblBeefPortions.Text = "Beef Portions";
            // 
            // lblChickenPortions
            // 
            this.lblChickenPortions.AutoSize = true;
            this.lblChickenPortions.Location = new System.Drawing.Point(12, 186);
            this.lblChickenPortions.Name = "lblChickenPortions";
            this.lblChickenPortions.Size = new System.Drawing.Size(87, 13);
            this.lblChickenPortions.TabIndex = 11;
            this.lblChickenPortions.Text = "Chicken Portions";
            // 
            // lblCalculatedPrice
            // 
            this.lblCalculatedPrice.AutoSize = true;
            this.lblCalculatedPrice.Location = new System.Drawing.Point(12, 216);
            this.lblCalculatedPrice.Name = "lblCalculatedPrice";
            this.lblCalculatedPrice.Size = new System.Drawing.Size(34, 13);
            this.lblCalculatedPrice.TabIndex = 12;
            this.lblCalculatedPrice.Text = "Price:";
            // 
            // lblMushroomPortionsAmount
            // 
            this.lblMushroomPortionsAmount.AutoSize = true;
            this.lblMushroomPortionsAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMushroomPortionsAmount.Location = new System.Drawing.Point(140, 96);
            this.lblMushroomPortionsAmount.Name = "lblMushroomPortionsAmount";
            this.lblMushroomPortionsAmount.Size = new System.Drawing.Size(109, 13);
            this.lblMushroomPortionsAmount.TabIndex = 14;
            this.lblMushroomPortionsAmount.Text = "mushroomPortions";
            // 
            // lblOnionPortionsAmount
            // 
            this.lblOnionPortionsAmount.AutoSize = true;
            this.lblOnionPortionsAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOnionPortionsAmount.Location = new System.Drawing.Point(140, 126);
            this.lblOnionPortionsAmount.Name = "lblOnionPortionsAmount";
            this.lblOnionPortionsAmount.Size = new System.Drawing.Size(84, 13);
            this.lblOnionPortionsAmount.TabIndex = 15;
            this.lblOnionPortionsAmount.Text = "onionPortions";
            // 
            // lblBeefPortionsAmount
            // 
            this.lblBeefPortionsAmount.AutoSize = true;
            this.lblBeefPortionsAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBeefPortionsAmount.Location = new System.Drawing.Point(140, 156);
            this.lblBeefPortionsAmount.Name = "lblBeefPortionsAmount";
            this.lblBeefPortionsAmount.Size = new System.Drawing.Size(78, 13);
            this.lblBeefPortionsAmount.TabIndex = 16;
            this.lblBeefPortionsAmount.Text = "beefPortions";
            // 
            // lblChickenPortionsAmount
            // 
            this.lblChickenPortionsAmount.AutoSize = true;
            this.lblChickenPortionsAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChickenPortionsAmount.Location = new System.Drawing.Point(140, 186);
            this.lblChickenPortionsAmount.Name = "lblChickenPortionsAmount";
            this.lblChickenPortionsAmount.Size = new System.Drawing.Size(98, 13);
            this.lblChickenPortionsAmount.TabIndex = 17;
            this.lblChickenPortionsAmount.Text = "chickenPortions";
            // 
            // lblPriceAmount
            // 
            this.lblPriceAmount.AutoSize = true;
            this.lblPriceAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPriceAmount.Location = new System.Drawing.Point(143, 216);
            this.lblPriceAmount.Name = "lblPriceAmount";
            this.lblPriceAmount.Size = new System.Drawing.Size(36, 13);
            this.lblPriceAmount.TabIndex = 18;
            this.lblPriceAmount.Text = "Price";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Crust size and thickness";
            // 
            // comboPizzaCrustType
            // 
            this.comboPizzaCrustType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboPizzaCrustType.FormattingEnabled = true;
            this.comboPizzaCrustType.Location = new System.Drawing.Point(285, 39);
            this.comboPizzaCrustType.Name = "comboPizzaCrustType";
            this.comboPizzaCrustType.Size = new System.Drawing.Size(95, 21);
            this.comboPizzaCrustType.TabIndex = 21;
            this.comboPizzaCrustType.SelectedIndexChanged += new System.EventHandler(this.comboPizzaCrustType_SelectedIndexChanged);
            // 
            // comboPizzaSize
            // 
            this.comboPizzaSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboPizzaSize.FormattingEnabled = true;
            this.comboPizzaSize.Location = new System.Drawing.Point(211, 39);
            this.comboPizzaSize.Name = "comboPizzaSize";
            this.comboPizzaSize.Size = new System.Drawing.Size(68, 21);
            this.comboPizzaSize.TabIndex = 22;
            this.comboPizzaSize.SelectedIndexChanged += new System.EventHandler(this.comboPizzaSize_SelectedIndexChanged);
            // 
            // btnSavePizza
            // 
            this.btnSavePizza.Location = new System.Drawing.Point(267, 309);
            this.btnSavePizza.Name = "btnSavePizza";
            this.btnSavePizza.Size = new System.Drawing.Size(115, 23);
            this.btnSavePizza.TabIndex = 23;
            this.btnSavePizza.Text = "Save Your Pizza";
            this.btnSavePizza.UseVisualStyleBackColor = true;
            this.btnSavePizza.Click += new System.EventHandler(this.btnSavePizza_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 357);
            this.Controls.Add(this.btnSavePizza);
            this.Controls.Add(this.comboPizzaSize);
            this.Controls.Add(this.comboPizzaCrustType);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblPriceAmount);
            this.Controls.Add(this.lblChickenPortionsAmount);
            this.Controls.Add(this.lblBeefPortionsAmount);
            this.Controls.Add(this.lblOnionPortionsAmount);
            this.Controls.Add(this.lblMushroomPortionsAmount);
            this.Controls.Add(this.lblCalculatedPrice);
            this.Controls.Add(this.lblChickenPortions);
            this.Controls.Add(this.lblBeefPortions);
            this.Controls.Add(this.lblOnionPortions);
            this.Controls.Add(this.lblMushroomPortions);
            this.Controls.Add(this.btnCalculatePrice);
            this.Controls.Add(this.btnCreateNewPizza);
            this.Controls.Add(this.btnAddChicken);
            this.Controls.Add(this.btnAddBeef);
            this.Controls.Add(this.btnAddOnion);
            this.Controls.Add(this.btnAddMushroom);
            this.Name = "Form1";
            this.Text = "PizzaBit";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnAddMushroom;
        private System.Windows.Forms.Button btnAddOnion;
        private System.Windows.Forms.Button btnAddBeef;
        private System.Windows.Forms.Button btnAddChicken;
        private System.Windows.Forms.Button btnCreateNewPizza;
        private System.Windows.Forms.Button btnCalculatePrice;
        private System.Windows.Forms.Label lblMushroomPortions;
        private System.Windows.Forms.Label lblOnionPortions;
        private System.Windows.Forms.Label lblBeefPortions;
        private System.Windows.Forms.Label lblChickenPortions;
        private System.Windows.Forms.Label lblCalculatedPrice;
        private System.Windows.Forms.Label lblMushroomPortionsAmount;
        private System.Windows.Forms.Label lblOnionPortionsAmount;
        private System.Windows.Forms.Label lblBeefPortionsAmount;
        private System.Windows.Forms.Label lblChickenPortionsAmount;
        private System.Windows.Forms.Label lblPriceAmount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboPizzaCrustType;
        private System.Windows.Forms.ComboBox comboPizzaSize;
        private System.Windows.Forms.Button btnSavePizza;
    }
}

